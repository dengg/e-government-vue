import Vue from 'vue'
import App from './App.vue'
import Storage from 'vue-ls'
import router from './router'
import store from './store/'

import { VueAxios } from "@/utils/request"
import { getAction,postAction} from '@/api'
import Antd from 'ant-design-vue'
import Viser from 'viser-vue'
import 'ant-design-vue/dist/antd.less';
import '@/permission' // permission control权限控制
import Axios from "axios"
Vue.prototype.$axios = Axios
Vue.prototype.postRequest = postAction
Vue.prototype.getRequest = getAction
import {
  ACCESS_TOKEN,
  CONFIG_STORAGE
} from "@/store/mutation-types"
import hasPermission from '@/utils/hasPermission'

Vue.config.productionTip = false
Vue.use(Storage, CONFIG_STORAGE.storageOptions)
Vue.use(Antd)
Vue.use(VueAxios, router)
Vue.use(Viser)
Vue.use(hasPermission)

new Vue({
  router,
  store,
  mounted () {
    store.commit('SET_TOKEN', Vue.ls.get(ACCESS_TOKEN))
  },
  render: h => h(App)
}).$mount('#app')
