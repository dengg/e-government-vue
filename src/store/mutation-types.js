export const ACCESS_TOKEN = 'Access-Token'
export const USER_NAME = 'Login_Username'
export const USER_INFO = 'Login_Userinfo'
export const ICONS = [{ name: 'check-circle-o' }, { name: 'profile' }, { name: 'message' }, { name: 'dribbble' }, { name: 'heart' }, { name: 'qrcode' },
  { name: 'dashboard' }, { name: 'table' }, { name: 'user' }, { name: 'warning' }, { name: 'setting' }, { name: 'form' },
  { name: 'cloud' }, { name: 'bar-chart' }, { name: 'inbox' }, { name: 'schedule' }, { name: 'compass' }, { name: 'aliwangwang' },
  { name: 'smile' }, { name: 'code' }, { name: 'solution' },{ name: 'switcher'}]

export const CONFIG_STORAGE = {
  // vue-ls options
  storageOptions: {
    namespace: 'pro__', // key prefix
    name: 'ls', // name variable Vue.[ls] or this.[$ls],
    storage: 'local', // storage name session, local, memory
  }
}