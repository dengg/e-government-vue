import Vue from 'vue'
import {login, logout} from "@/api/login"
import { queryPermissionsByUser } from "@/api"
import { ACCESS_TOKEN, USER_NAME,USER_INFO } from "@/store/mutation-types"
import { welcome , treeUtils } from "@/utils/util"

const user = {
  state: {
    token: '',
    username: '',
    realname: '',
    welcome: '',
    avatar: '',
    permissionList: [],
    info: {},
    menuShow:''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, { username, realname, welcome }) => {
      state.username = username
      state.realname = realname
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_PERMISSIONLIST: (state, permissionList) => {
      state.permissionList = permissionList
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    SET_MENU_SHOW:(state, menuShow) => {
      state.menuShow = menuShow
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
          if (response.Code == '1') {
            // const result = response.result
            const userInfo = response.Data.BusUser;
            userInfo.avatar = "";
            userInfo.realname = userInfo.Ur_Name;
            Vue.ls.set(ACCESS_TOKEN, response.Ticket, 7 * 24 * 60 * 60 * 1000)
            Vue.ls.set(USER_NAME, userInfo.Ur_Name, 7 * 24 * 60 * 60 * 1000)
            Vue.ls.set(USER_INFO, userInfo, 7 * 24 * 60 * 60 * 1000)
            commit('SET_TOKEN', response.Ticket)
            commit('SET_INFO', userInfo)
            commit('SET_NAME', { username: userInfo.Ur_Name,realname: userInfo.Ur_Name, welcome: welcome() })
            commit('SET_AVATAR', userInfo.avatar)
            resolve()
          }else{
            reject(response)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetPermissionList({ commit }) {
      return new Promise((resolve, reject) => {
        queryPermissionsByUser().then(res => {
          const menuData = treeUtils(res);
          if (menuData && menuData.length > 0) {
            commit('SET_PERMISSIONLIST', menuData)
          } else {
            reject('getPermissionList: permissions must be a non-null array !')
          }
          resolve(menuData)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    Logout({ commit, state }) {
      return new Promise((resolve) => {
        let logoutToken = state.token;
        commit('SET_TOKEN', '')
        commit('SET_PERMISSIONLIST', [])
        Vue.ls.remove(ACCESS_TOKEN)
        //console.log('logoutToken: '+ logoutToken)
        logout(logoutToken).then(() => {
          resolve()
        }).catch(() => {
          resolve()
        })
      })
    },

    setMenuShow: ({ commit }, menuShow) => {
      commit('SET_MENU_SHOW', menuShow)
    }

  }
}

export default user
